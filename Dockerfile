ARG REPO_ARCH

# Using Alpine to keep the images smaller
FROM $REPO_ARCH/node:16-alpine

# Pushing all files into image
WORKDIR /app
ADD https://github.com/nolanlawson/pinafore/archive/vPINAFORE_VERSION.tar.gz /app/pinafore.tar.gz
RUN \
  tar zxf pinafore.tar.gz && \
  rm pinafore.tar.gz

WORKDIR /app/pinafore-PINAFORE_VERSION

# Install Pinafore
RUN yarn --production --pure-lockfile --network-timeout=30000 \
  && yarn build --network-timeout=30000 \
  && yarn cache clean \
  && rm -rf ./src ./docs ./tests ./bin

# Expose port 4002
EXPOSE 4002

# Setting run-command, using explicit `node` command
# rather than `yarn` or `npm` to use less memory
# https://github.com/nolanlawson/pinafore/issues/971
CMD PORT=4002 node server.js
